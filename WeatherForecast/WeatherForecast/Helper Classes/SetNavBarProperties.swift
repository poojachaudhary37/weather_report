//
//  SetNavBarProperties.swift
//  WeatherForecast
//
//  Created by pooja on 24/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import Foundation
import UIKit

protocol SetNavBarProperties {
    func setNavBarProperties()
}

extension SetNavBarProperties where Self: UIViewController {
    func setNavBarProperties() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 95/255, green: 76/255, blue: 76/255, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
}
