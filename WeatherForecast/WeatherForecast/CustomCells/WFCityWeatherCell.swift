//
//  CityWeatherCell.swift
//  WeatherForecast
//
//  Created by pooja on 24/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import UIKit

class WFCityWeatherCell: UITableViewCell {

    //MARK:- iVars
    @IBOutlet weak var seasonImage: UIImageView!
    @IBOutlet weak var cityNameLbl: UILabel!{
        didSet {
            cityNameLbl.font = UIFont.systemFont(ofSize: 16)
        }
    }
    @IBOutlet weak var tempLbl: UILabel!{
        didSet {
            tempLbl.font = UIFont.systemFont(ofSize: 16)
        }
    }
    var weatherData: WFWeatherDataSource?{
        didSet {
            if let wd = weatherData {
                self.cityNameLbl.text = wd.name
                self.tempLbl.text = "\(wd.mainData.temp)".withTempUnit()
            }
        }
    }
    
    //MARK:- Overridden functions
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //MARK:- Internal class func
    class func reuseIdentifier() -> String {
        return "WFCityWeatherCell"
    }

}
