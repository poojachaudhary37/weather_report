//
//  WeatherDataModel.swift
//  WeatherForecast
//
//  Created by pooja on 24/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import Foundation
import SwiftyJSON

struct WFWeatherDetails {
    private struct SerializableKeys {
        static let kCountKey = "cnt"
        static let kListKey = "list"
    }
    var response: JSON
    var count: Any
    var dataSource: [WFWeatherDataSource] = [WFWeatherDataSource]()
    
    init(resp: JSON) {
        self.response = resp
        self.count = resp[SerializableKeys.kCountKey].int!
        (resp[SerializableKeys.kListKey].arrayObject as! [[String: Any]]).forEach { data in
            self.dataSource.append(WFWeatherDataSource.init(resp: data))
        }
    }
}
