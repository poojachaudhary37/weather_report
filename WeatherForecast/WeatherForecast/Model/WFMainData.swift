//
//  WFMainData.swift
//  WeatherForecast
//
//  Created by pooja on 24/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import Foundation

struct WFMainData {
    private struct SerializableKeys {
        static let kTempKey = "temp"
        static let kPressureKey = "pressure"
        static let kHumidityKey = "humidity"
        static let kTempMinKey = "temp_min"
        static let kTempMaxKey = "temp_max"
    }
    
    var response: [String: Any]
    var temp: Double
    var pressure, humidity, temp_min, temp_max: Int
    
    init(resp: [String: Any]) {
        self.response = resp
        self.temp = resp[SerializableKeys.kTempKey] as! Double
        self.pressure = resp[SerializableKeys.kPressureKey] as! Int
        self.humidity = resp[SerializableKeys.kHumidityKey] as! Int
        self.temp_min = resp[SerializableKeys.kTempMinKey] as! Int
        self.temp_max = resp[SerializableKeys.kTempMaxKey] as! Int
    }
}
