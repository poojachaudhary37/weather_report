//
//  WFWeatherData.swift
//  WeatherForecast
//
//  Created by pooja on 24/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import Foundation

struct WFWeatherData {
    private struct SerializableKeys {
        static let kIDKey = "id"
        static let kMainKey = "main"
        static let kDescKey = "description"
        static let kIconKey = "icon"
    }
    var response: [String: Any]
    var id: Int
    var main, description, icon: String
    
    init(resp: [String: Any]) {
        self.response = resp
        self.id = resp[SerializableKeys.kIDKey] as! Int
        self.main = resp[SerializableKeys.kMainKey] as! String
        self.description = resp[SerializableKeys.kDescKey] as! String
        self.icon = resp[SerializableKeys.kIconKey] as! String
    }
}
