//
//  WFSysData.swift
//  WeatherForecast
//
//  Created by pooja on 24/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import Foundation

struct WFCoordData {
    private struct SerializableKeys {
        static let kLatKey = "lat"
        static let kLongKey = "lon"
    }
    var response: [String: Double]
    var latitude, longitude: Double
    
    init(resp: [String: Double]) {
        self.response = resp
        self.latitude = resp[SerializableKeys.kLatKey]!
        self.longitude = resp[SerializableKeys.kLongKey]!
    }
}
