//
//  WFSysData.swift
//  WeatherForecast
//
//  Created by pooja on 24/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import Foundation

struct WFSysData {
    private struct SerializableKeys {
        static let kTypeKey = "type"
        static let kIDKey = "id"
        static let kMessageKey = "message"
        static let kCountryKey = "country"
        static let kSunriseKey = "sunrise"
        static let kSunsetKey = "sunset"
    }
    var response: [String: Any]
    var type, id: Int
    var message: Double
    var country: String
    var sunrise, sunset: Int64
    init(resp: [String: Any]) {
        self.response = resp
        self.type = resp[SerializableKeys.kTypeKey] as! Int
        self.id = resp[SerializableKeys.kIDKey] as! Int
        self.message = resp[SerializableKeys.kMessageKey] as! Double
        self.country = resp[SerializableKeys.kCountryKey] as! String
        self.sunrise = resp[SerializableKeys.kSunriseKey] as! Int64
        self.sunset = resp[SerializableKeys.kSunsetKey] as! Int64
    }
}
