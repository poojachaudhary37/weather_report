//
//  WFWindData.swift
//  WeatherForecast
//
//  Created by pooja on 24/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import Foundation

struct WFWindData {
    private struct SerializableKeys {
        static let kSpeedKey = "speed"
        //static let kDegKey = "deg"
    }
    
    var response: [String: Any]
    var speed: Double
    //var deg: Double
    
    init(resp: [String: Any]) {
        self.response = resp
        self.speed = resp[SerializableKeys.kSpeedKey] as! Double
        //self.deg = resp[SerializableKeys.kDegKey] as! Double
    }
}
