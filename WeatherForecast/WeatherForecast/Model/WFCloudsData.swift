//
//  WFCloudsData.swift
//  WeatherForecast
//
//  Created by pooja on 24/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import Foundation

struct WFCloudsData {
    private struct SerializableKeys {
        static let kAllKey = "all"
    }
    
    var response: [String: Int]
    var all: Int
    
    init(resp: [String: Int]) {
        self.response = resp
        self.all = resp[SerializableKeys.kAllKey]!
    }
}
