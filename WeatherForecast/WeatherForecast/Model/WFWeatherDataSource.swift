//
//  WeatherData.swift
//  WeatherForecast
//
//  Created by pooja on 24/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import Foundation

struct WFWeatherDataSource {
    private struct SerializableKeys {
        static let kCoordKey = "coord"
        static let kSysKey = "sys"
        static let kWeatherKey = "weather"
        static let kMainKey = "main"
        static let kVisibilityKey = "visibility"
        static let kWindKey = "wind"
        static let kCloudsKey = "clouds"
        static let kDtKey = "dt"
        static let kIDKey = "id"
        static let kNameKey = "name"
    }
    var response: [String: Any]
    var coordData: WFCoordData
    var sysData: WFSysData
    var weatherData: [WFWeatherData] = [WFWeatherData]()
    var mainData: WFMainData
    var windData: WFWindData
    var cloudsData: WFCloudsData
    var visibility, dt, id: Int64
    var name: String
    
    init(resp : [String: Any]) {
        self.response = resp
        self.coordData = WFCoordData.init(resp: resp[SerializableKeys.kCoordKey] as! [String : Double])
        self.sysData = WFSysData.init(resp: resp[SerializableKeys.kSysKey] as! [String: Any])
        self.mainData = WFMainData.init(resp: resp[SerializableKeys.kMainKey]  as! [String: Any])
        self.windData = WFWindData.init(resp: resp[SerializableKeys.kWindKey]  as! [String: Any])
        self.cloudsData = WFCloudsData.init(resp: resp[SerializableKeys.kCloudsKey] as! [String : Int])
        self.visibility = resp[SerializableKeys.kVisibilityKey] as! Int64
        self.dt = resp[SerializableKeys.kDtKey] as! Int64
        self.id = resp[SerializableKeys.kIDKey] as! Int64
        self.name = resp[SerializableKeys.kNameKey] as! String
        (resp[SerializableKeys.kWeatherKey] as! [[String: Any]]).forEach { w in
            self.weatherData.append(WFWeatherData.init(resp: w))
        }
    }
}
