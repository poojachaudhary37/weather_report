//
//  AppConstant.swift
//  WeatherForecast
//
//  Created by pooja on 24/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import UIKit

class WFAppConstants {
    static let forecastAPIKey = "d5b7579edbaeee01bf96bd34f6b0a155"
    static let baseWeatherURL = "http://api.openweathermap.org/data/2.5/group?"
}
