//
//  String+Extension.swift
//  WeatherForecast
//
//  Created by pooja on 24/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import Foundation

extension String {
    func withTempUnit() -> String {
        return self + " " + "°C"
    }
}
