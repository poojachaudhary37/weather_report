//
//  WFDetailView.swift
//  WeatherForecast
//
//  Created by pooja on 25/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import UIKit

class WFDetailView: UIViewController,SetNavBarProperties {
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    var weatherData: WFWeatherDataSource?

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavBarProperties()

        var gifImage = UIImage.gifImageWithName("cloudy_weather")

        if let wd = weatherData {
            self.cityName.text = wd.name
            self.tempLabel.text = "\(wd.mainData.temp)".withTempUnit()
            self.humidityLabel.text = "\(wd.mainData.humidity)"
            self.pressureLabel.text = "\(wd.mainData.pressure)"
            self.sunriseLabel.text = "\(wd.mainData.temp_min)".withTempUnit()
            self.sunsetLabel.text = "\(wd.mainData.temp_max)".withTempUnit()
            self.descriptionLabel.text = wd.weatherData.first?.description
            self.windLabel.text = "\(wd.windData.speed)"
            
            let weather_str = self.descriptionLabel.text as! String
            if weather_str.range(of:"clear") != nil {
                gifImage = UIImage.gifImageWithName("sunny_weather")
                weatherImage.image = gifImage
            }
            else if weather_str.range(of:"cloud") != nil
            {
                gifImage = UIImage.gifImageWithName("cloudly")
                weatherImage.image = gifImage
            }
            else if weather_str.range(of:"rain") != nil
            {
                gifImage = UIImage.gifImageWithName("rain_weather")
                weatherImage.image = gifImage
            }
            else{
                gifImage = UIImage.gifImageWithName("cloudy_weather")
                weatherImage.image = gifImage
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
