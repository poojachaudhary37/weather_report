//
//  ApiManager.swift
//  WeatherForecast
//
//  Created by pooja on 24/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

enum Response<T> {
    case response(T)
}
class WFApiManager: NSObject {
    
    class func getWeatherForecast(cityId: String, completionHandler: @escaping (_ response: Response<Any>) -> ()) {
        let postString = "id=\(cityId)&units=metric&APPID="+WFAppConstants.forecastAPIKey
        let apiURL = WFAppConstants.baseWeatherURL + postString
        Alamofire.request(apiURL, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case let .success(value):
                let json = JSON(value)
                completionHandler(.response(json))
            case .failure(let error):
                completionHandler(.response(error))
            }
        }
    }
}

