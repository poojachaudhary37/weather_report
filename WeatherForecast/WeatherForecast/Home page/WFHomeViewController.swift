//
//  WFHomeViewController.swift
//  WeatherForecast
//
//  Created by pooja on 24/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import UIKit
import SwiftyJSON

class WFHomeViewController: UITableViewController, SetNavBarProperties {

    //MARK:- iVars
    let cityId = "4163971,2147714,2174003"
    var weatherDetails: WFWeatherDetails?
    
    //MARK:- Overridden ViewController functions
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib.init(nibName: WFCityWeatherCell.reuseIdentifier(), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: WFCityWeatherCell.reuseIdentifier())
        tableView.tableFooterView = UIView()
        refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        refreshControl?.beginRefreshing()
        setNavBarProperties()
        getWeatherData(cityID: cityId)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private functions
    @objc private func refreshData() {
        getWeatherData(cityID: cityId)
    }
    private func getWeatherData(cityID: String) {
        WFApiManager.getWeatherForecast(cityId: cityID, completionHandler: { [weak self] result in
            self?.refreshControl?.endRefreshing()
            switch result {
            case .response(let res):
                if res is Error {
                    let alertController = UIAlertController(title: "Weather Forecast", message: "\((res as! Error).localizedDescription)", preferredStyle: .alert)
                    let okAct = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alertController.addAction(okAct)
                    self?.present(alertController, animated: true, completion: nil)
                }else {
                    self?.weatherDetails = WFWeatherDetails.init(resp: res as! JSON)
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                }
            }
        })
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        guard let data = weatherDetails else {return 0}
        return data.dataSource.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: WFCityWeatherCell.reuseIdentifier(), for: indexPath) as? WFCityWeatherCell else {return UITableViewCell()}
        cell.selectionStyle = .none
        cell.weatherData = self.weatherDetails?.dataSource[indexPath.row]
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let DetailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailVC") as! WFDetailView
        DetailVC.weatherData = self.weatherDetails?.dataSource[indexPath.row]
       self.navigationController?.pushViewController(DetailVC, animated: true)
    }
}
