//
//  WFHomeViewControllerTest.swift
//  WeatherForecastTests
//
//  Created by pooja on 25/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//
import UIKit
import XCTest
@testable import WeatherForecast

var vc: WFHomeViewController?

class WFHomeViewControllerTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        vc = storyboard.instantiateViewController(withIdentifier: "WFHomeViewController") as! WFHomeViewController
        vc.performSelector(onMainThread: #selector(self.loadView), with: nil, waitUntilDone: true)

        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    func testThatViewLoads() {
        XCTAssertNotNil(vc?.view, "View not initiated properly")
    }
    
    func testThatTableViewLoads() {
        XCTAssertNotNil(vc?.tableView, "TableView not initiated")
    }
    
    // MARK: - UITableView tests
    func testThatViewConformsToUITableViewDataSource() {
        XCTAssertTrue(vc != nil, "View does not conform to UITableView datasource protocol")
    }
    
    func testThatTableViewHasDataSource() {
        XCTAssertNotNil(vc?.tableView.dataSource, "Table datasource cannot be nil")
    }
    
    func testThatViewConformsToUITableViewDelegate() {
        XCTAssertTrue(vc != nil, "View does not conform to UITableView delegate protocol")
    }
    
    func testTableViewIsConnectedToDelegate() {
        XCTAssertNotNil(vc?.tableView.delegate, "Table delegate cannot be nil")
    }
    func testTableViewCellCreateCellsWithReuseIdentifier() {
        let indexPath = IndexPath(row: 0, section: 0)
        let cell: UITableViewCell? = vc?.tableView((vc?.tableView)!, cellForRowAt: indexPath)
        let expectedReuseIdentifier = "\(Int(indexPath.section))/\(Int(indexPath.row))"
        XCTAssertTrue((cell?.reuseIdentifier == expectedReuseIdentifier), "Table does not create reusable cells")
    }

   override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
   func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
